NAME = andriyg/phpfcgi_nginx
VERSION = latest

all: build

build:
	docker build -t $(NAME):$(VERSION) --rm .

push: build
	docker push $(NAME):$(VERSION)
