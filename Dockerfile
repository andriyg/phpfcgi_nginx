FROM andriyg/ubuntu:trusty

RUN \
    apt-key adv --keyserver hkp://pgp.mit.edu:80 --recv-keys 573BFD6B3D8FBC641079A6ABABF5BD827BD9BF62 && \
    echo "deb http://nginx.org/packages/mainline/ubuntu/ trusty nginx" >> /etc/apt/sources.list && \
    apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xcbcb082a1bb943db && \
    echo "deb [arch=amd64,i386] http://ams2.mirrors.digitalocean.com/mariadb/repo/10.1/ubuntu trusty main" > /etc/apt/sources.list.d/mariadb.list && \
    apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y ca-certificates nginx mariadb-client postgresql-client php-apc php-db php-gettext php-pear php5-cgi php5-cli php5-dev php5-gd php5-mcrypt php5-mysql php5-pgsql supervisor wget curl && \
    cd /tmp && \
    wget http://pecl.php.net/get/htscanner-1.0.1.tgz && \
    tar xzvf htscanner-1.0.1.tgz && \
    cd htscanner-1.0.1/ && \
    ls && \
    phpize && \
    ./configure && \
    make && \
    make install && \
    echo "extension=htscanner.so" > /etc/php5/cli/conf.d/99-htscanner.ini && \
    grep . /etc/php5/*/conf.d/99-htscanner.ini && \
    php -i | grep hts && \
    apt-get remove -y wget php5-dev build-essential cpp gcc && \
    apt-get autoremove -y && \
    apt-get autoclean -y && \
    rm -rf /tmp/* && \
    rm -rf /var/lib/apt/lists/* && \
    true

ADD nginx /etc/nginx

RUN \
    sed -i -e"s/keepalive_timeout  65/keepalive_timeout  65;\n\tclient_max_body_size 20m/" /etc/nginx/nginx.conf && \
    echo "daemon off;" >> /etc/nginx/nginx.conf && \
    true

ADD supervisor /etc/supervisor

ADD start.sh /

EXPOSE 80 443

CMD ["/bin/bash", "/start.sh"]
